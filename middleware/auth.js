export default async function (context) {
  if (process.client) {
    const {$axios, route, redirect, store} = context;

    const api_token = localStorage.getItem('api_token');
    if (api_token) {
      $axios.setToken(api_token, 'Bearer')
    } else {
      $axios.setToken(false)
    }

    try {
      const token = await store.dispatch('auth/check');
      if (token) {
        return token;
      }else {
        redirect({name: 'account-login', params: {redirect: route}});
      }
    } catch (e) {
      redirect({name: 'account-login', params: {redirect: route}});
    }
  }
}
