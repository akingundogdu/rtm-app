export default function ({$axios, redirect, route}) {
  $axios.onRequest(config => {
    if (process.client) {
      let api_token = route.query.api_token;
      if (api_token) {
        localStorage.setItem('api_token', api_token);
      }

      api_token = localStorage.getItem('api_token');
      if (api_token) {
        $axios.setToken(api_token, 'Bearer');
      } else {
        $axios.setToken(false)
      }
    }
  });

  $axios.onError(e => {
    if (e.response && e.response.status === 401) {
      redirect({name: 'account-login', params: {redirect: route}});
    } else {
      throw e;
    }
  });
  $axios.setBaseURL(process.env.BASE_URL);
  $axios.setHeader('Accept', 'application/json');
}
