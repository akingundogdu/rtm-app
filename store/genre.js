import {getField, updateField} from 'vuex-map-fields';

export default {
    state: () => ({
        modal: false,
        genres: [],
        followed_genres: [],
        all_genres: []
    }),
    getters: {
        getField,
        GET_GENRES(state) {
            return state.genres;
        }
    },
    mutations: {
        updateField,
        SET_MODAL(state, value) {
            state.modal = value;
        },
        SET_LOADING(state, {id, loading}) {
            if (state.genres) {
                const genre = findGenresBy(state, {id});
                genre.loading = loading;
            }
        },
        SET_FOLLOW(state, {id}) {
            if (state.genres) {
                const genre = findGenresBy(state, {id});
                genre.follow = !genre.follow;
            }
        },
        SET_ALL_GENRES(state, value) {
            state.all_genres = value;
        },
        SET_FOLLOWED_GENRES(state, value) {
            state.followed_genres = value;
        },
        SET_GENRES(state, value) {
            state.genres = value;
        }
    },
    actions: {
        /**
         * @return {boolean}
         */
        async FOLLOW({state, commit}, {id}) {
            commit('SET_LOADING', {id, loading: true});
            const authToken = localStorage.getItem('api_token');
            const data = await this.$axios.$post('/graphql', {
                query: `mutation {
                          followGenre(input: {token: "${authToken}", genreId: ${id}})
                          {
                            errors
                            success
                          }
                        }
                       `
            });
            if (data.data && data.data.followGenre && data.data.followGenre.success) {
                commit('SET_FOLLOW', {id});
            }
            setInterval(function () {
                commit('SET_LOADING', {id, loading: false});
            },500);
        }
        ,
        /**
         * @return {boolean}
         */
        async FETCH({state, commit}) {
            const authToken = localStorage.getItem('api_token');
            const data = await this.$axios.$post('/graphql', {
                query: `query {
                            user(token: "${authToken}"){
                                email
                                id
                                genres { 
                                    id 
                                    name 
                                    
                                }
                            }
                            genres{
                                id
                                name
                            }
                        }
                       `
            });
            if (data.data && data.data.user && data.data.user.genres && data.data.genres) {
                commit('SET_ALL_GENRES', data.data.genres);
                commit('SET_FOLLOWED_GENRES', data.data.user.genres);


                const genres = [];
                state.all_genres.forEach((genre, index) => {
                    const user_genre = state.followed_genres.find(value => {
                        return value.id === genre.id;
                    });
                    let follow = false;
                    if (user_genre) {
                        follow = true;
                    }

                    genres.push({
                        id: genre.id,
                        follow: follow,
                        loading: false,
                        title: genre.name
                    });
                });
                commit('SET_GENRES', genres);
            }
        }

    }
}

function findGenresBy(state, {id}) {
    return state.genres.find(genre => {
        return genre.id === id;
    });
}