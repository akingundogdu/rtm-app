import {getField, updateField} from 'vuex-map-fields';

export default {
    state: () => ({
        form: {
            first_name: null,
            last_name: null,
            email: null,
            password: null,
            password_confirmation: null
        },
        errors: {
            email: null
        }
    }),
    getters: {
        getField,
    },
    mutations: {
        updateField,
        SET_ERROR(state, value) {
            state.errors.email = value;
        }
    },
    actions: {
        /**
         * @return {boolean}
         */
        async REGISTER({state, commit}) {
            this.$axios.setToken('-', 'Bearer');
            const data = await this.$axios.$post('/graphql', {
                query: `mutation {
                            registerUser(input: {
                            firstName: "${state.form.first_name}", 
                            lastName: "${state.form.last_name}", 
                            email: "${state.form.email}",
                            password: "${state.form.password}", 
                            passwordConfirmation: "${state.form.password_confirmation}"}) {
                            user {
                              email
                              authenticationToken
                            }
                            errors
                          }}
                        `
            });

            if (data && data.errors) {
                commit('SET_ERROR', data.errors[0].message);
            }else {
                const authToken = data.data.registerUser.user.authenticationToken;
                if (authToken)
                {
                    localStorage.setItem('api_token', authToken.toString());
                    this.$axios.setToken(authToken, 'Bearer');
                    return true;
                }
            }
            return false;
        }
    }
}
