
export default {
    state: () => ({}),
    mutations: {},
    actions: {
        async check({commit}) {
            const data = await this.$axios.$post('/graphql', {
                query:
                    `{
                        check
                     }`
            });
            if (!data.check && localStorage.getItem('api_token') !== data.data.check) {
                localStorage.clear();
                return null;
            }
            return true;
        }
    },
    getters: {}
}
