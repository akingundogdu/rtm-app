import {getField, updateField} from 'vuex-map-fields';

export default {
    state: () => ({
        modal: false
    }),
    getters: {
        getField,
    },
    mutations: {
        updateField,
        SET_MODAL(state, value) {
            state.modal = value;
        }
    },
    actions: {
        /**
         * @return {boolean}
         */
        async FETCH({state, commit}) {

        }
    }
}
