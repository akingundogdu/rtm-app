import {getField, updateField} from 'vuex-map-fields';

export default {
    state: () => ({
        form: {
            email: '',
            password: '',
        },
        errors: {
            email: null
        }
    }),
    getters: {
        getField,
    },
    mutations: {
        updateField,
        SET_ERROR(state, value) {
            state.errors.email = value;
        }
    },
    actions: {
        /**
         * @return {boolean}
         */
        async LOGIN({state, commit}) {
            this.$axios.setToken('-', 'Bearer');
            const data = await this.$axios.$post('/graphql', {
                query: `mutation {
                            signIn(input: { 
                            email: "${state.form.email}",
                            password: "${state.form.password}"}) {
                            user {
                              email
                              authenticationToken
                            }
                            errors
                          }}
                        `
            });
            if (data && data.errors) {
                commit('SET_ERROR', data.errors[0].message);
            }else {
                const authToken = data.data.signIn.user.authenticationToken;
                if (authToken)
                {
                    localStorage.setItem('api_token', authToken.toString());
                    this.$axios.setToken(authToken, 'Bearer');
                    return true;
                }
            }
            return false;

        }
    }
}
