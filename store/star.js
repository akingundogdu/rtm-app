import {getField, updateField} from 'vuex-map-fields';

export default {
    state: () => ({
        modal: false,
        stars: [],
        followed_stars: [],
        all_stars: []
    }),
    getters: {
        getField,
        GET_STARS(state) {
            return state.stars;
        }
    },
    mutations: {
        updateField,
        SET_MODAL(state, value) {
            state.modal = value;
        },
        SET_LOADING(state, {id, loading}) {
            if (state.stars) {
                const star = findStarsBy(state, {id});
                star.loading = loading;
            }
        },
        SET_FOLLOW(state, {id}) {
            if (state.stars) {
                const star = findStarsBy(state, {id});
                star.follow = !star.follow;
            }
        },
        SET_ALL_STARS(state, value) {
            state.all_stars = value;
        },
        SET_FOLLOWED_STARS(state, value) {
            state.followed_stars = value;
        },
        SET_STARS(state, value) {
            state.stars = value;
        }
    },
    actions: {
        /**
         * @return {boolean}
         */
        async FOLLOW({state, commit}, {id}) {
            commit('SET_LOADING', {id, loading: true});
            const authToken = localStorage.getItem('api_token');
            const data = await this.$axios.$post('/graphql', {
                query: `mutation {
                          followStar(input: {token: "${authToken}", starId: ${id}})
                          {
                            errors
                            success
                          }
                        }
                       `
            });
            if (data.data && data.data.followStar && data.data.followStar.success) {
                commit('SET_FOLLOW', {id});
            }
            setInterval(function () {
                commit('SET_LOADING', {id, loading: false});
            },500);
        }
        ,
        /**
         * @return {boolean}
         */
        async FETCH({state, commit}) {
            const authToken = localStorage.getItem('api_token');
            const data = await this.$axios.$post('/graphql', {
                query: `query {
                            user(token: "${authToken}"){
                                email
                                id
                                stars { 
                                    id 
                                    name 
                                    
                                }
                            }
                            stars{
                                id
                                name
                            }
                        }
                       `
            });
            if (data.data && data.data.user && data.data.user.stars && data.data.stars) {
                commit('SET_ALL_STARS', data.data.stars);
                commit('SET_FOLLOWED_STARS', data.data.user.stars);


                const stars = [];
                state.all_stars.forEach((star, index) => {
                    const user_star = state.followed_stars.find(value => {
                        return value.id === star.id;
                    });
                    let follow = false;
                    if (user_star) {
                        follow = true;
                    }

                    stars.push({
                        id: star.id,
                        follow: follow,
                        loading: false,
                        title: star.name,
                        avatar: getRandomImage()
                    });
                });
                commit('SET_STARS', stars);
            }
        }

    }
}

function findStarsBy(state, {id}) {
    return state.stars.find(star => {
        return star.id === id;
    });
}

function getRandomImage() {
    let random = Math.floor(Math.random() * 5)
    random = random < 0 ? 1 : random;
    return `https://cdn.vuetifyjs.com/images/lists/${random}.jpg`
}