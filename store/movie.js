import {getField, updateField} from 'vuex-map-fields';

export default {
    state: () => ({
        movies_by_year: [],
        movies_by_rate: [],
        movies_by_director: [],
        movies_by_suggestion: [],
        movies_followed: []
    }),
    getters: {
        getField,
    },
    mutations: {
        updateField,
        SET_MOVIES_BY_YEAR(state, value) {
            state.movies_by_year = value;
        },
        SET_MOVIES_BY_RATE(state, value) {
            state.movies_by_rate = value;
        },
        SET_MOVIES_BY_DIRECTOR(state, value) {
            state.movies_by_director = value;
        },
        SET_MOVIES_BY_FOLLOWED(state, value) {
            state.movies_followed = value;
        },
        SET_MOVIES_BY_SUGGESTION(state, value) {
            state.movies_by_suggestion = value;
        },
    },
    actions: {
        /**
         * @return {boolean}
         */
        async FOLLOW({state, commit, dispatch}, {id}) {
            const authToken = localStorage.getItem('api_token');
            const data = await this.$axios.$post('/graphql', {
                query: `mutation {
                          followMovie(input: {token: "${authToken}", movieId: ${id}})
                          {
                            errors
                            success
                          }
                        }
                       `
            });
            if (data.data && data.data.followMovie && data.data.followMovie.success) {
                dispatch('FETCH');
            }
        }
        ,
        /**
         * @return {boolean}
         */
        async FETCH({state, commit}) {
            const authToken = localStorage.getItem('api_token');
            const data = await this.$axios.$post('/graphql', {
                query: `query {
                            by_followed: user(token:"${authToken}"){
                                movies{
                                    id
                                    name
                                    summary
                                    rate
                                    year
                                    director
                                    time
                                    image
                                    genres{
                                       name
                                    }
                                }
                            }
                            by_suggestion: suggestions (token:"${authToken}"){
                                movie {
                                    id
                                    name
                                    summary
                                    rate
                                    year
                                    director
                                    time
                                    image
                                    genres{
                                       name
                                    }
                                }
                            }
                            by_year :movies(by:"year") {
                                    id
                                    name
                                    summary
                                    rate
                                    year
                                    director
                                    time
                                    image
                                    genres{
                                       name
                                    }
                            }
                            by_rate: movies(by:"rate") {
                                    id
                                    name
                                    summary
                                    rate
                                    year
                                    director
                                    time
                                    image
                                    genres{
                                       name
                                    }
                            }
                            by_director: movies(by:"director") {
                                    id
                                    name
                                    summary
                                    rate
                                    year
                                    director
                                    time
                                    image
                                    genres{
                                       name
                                    }
                            }
                        }

                       `
            });
            if (data.data) {
                commit('SET_MOVIES_BY_FOLLOWED', data.data.by_followed.movies);
                commit('SET_MOVIES_BY_SUGGESTION', data.data.by_suggestion);
                setMoviesBy(state, commit, data.data.by_followed.movies, 'SET_MOVIES_BY_FOLLOWED');
                setMoviesBy(state, commit, data.data.by_year, 'SET_MOVIES_BY_YEAR');
                setMoviesBy(state, commit, data.data.by_rate, 'SET_MOVIES_BY_RATE');
                setMoviesBy(state, commit, data.data.by_director, 'SET_MOVIES_BY_DIRECTOR');
            }
        }

    }
}

function setMoviesBy(state, commit, data, setter) {
    const movies = [];
    data.forEach((item) => {
        const movies_by = state.movies_followed.find(value => {
            return value.id === item.id;
        });
        let follow = false;
        if (movies_by) {
            follow = true;
        }

        movies.push({
            id: item.id,
            follow: follow,
            loading: false,
            name: item.name,
            summary: item.summary,
            rate: item.rate,
            year: item.year,
            director: item.director,
            time: item.time,
            image: item.image,
            genres: item.genres
        });
    });
    commit(setter, movies);
}