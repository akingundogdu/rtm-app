import {getField, updateField} from 'vuex-map-fields';

export default {
    state: () => ({
        form: {
            email: null,
            password: null
        },
        errors: {
            email: null
        }
    }),
    getters: {
        getField,
    },
    mutations: {
        updateField,
        SET_ERROR(state, value) {
            state.errors.email = value;
        }
    },
    actions: {
        /**
         * @return {boolean}
         */
        async CHANGE_PASSWORD({state, commit}) {
            this.$axios.setToken('-', 'Bearer');
            const data = await this.$axios.$post('/graphql', {
                query: `mutation {
                                changePassword(input:{email:"${state.form.email}", password: "${state.form.password}"}) {
                                    success
                                }
                            }
                        `
            });
            if (data && data.errors) {
                commit('SET_ERROR', data.errors[0].message);
            } else if (data && data.data.changePassword && data.data.changePassword.success) {
                localStorage.clear();
                return true;
            }
            return false;
        }
    }
}
