# Welcome to RTM Frontend app documentation



Remote team movie is a technical test project for senior software engineer role of remote team company. 

This project is divided into two as frontend and backend and consists of separate projects. We use Vue.js in client side.

  - You can access app online from [client-online]
  - You can access source code of client app from [client-source-code]
  - You can access backend api online from [backend-online] if you want
  - You can access source code of backend api from [backend-source-code]


# Prerequisites

>Before we begin, make sure you have [node.js](https://nodejs.org/en).


```sh
$ node --version # v12.10.0
```


### Npm
As you know client side utilizes [npm](https://www.npmjs.com) to manage its dependencies. So, before starting setup RTM client app, make sure you have [npm](https://www.npmjs.com) installed on your machine.

You can check if you have 
```sh
$ which npm
```

To update to its latest version with:

```sh
$ npm update
```

# Project Setup

First step, we need to clone frontend app repo to your local machine.
```sh
$ git clone https://gitlab.com/akingundogdu/rtm-app.git
```
After that, go to inside directory your cloned path and running this command.
```sh
$ npm install
```

### Dependencies
Let's take a moment to review the gems that we used.



- [Vue.js](https://vuejs.org) - is a progressive framework for building user interfaces.
- [Vuex](https://vuex.vuejs.org/guide) - Vuex is a state management pattern + library for Vue.js applications
- [Nuxt](https://nuxtjs.org) - Nuxt is a framework designed to give you a strong architecture following official Vue guidelines.
- [vuex-map-fields](https://www.npmjs.com/package/vuex-map-fields?activeTab=readme) - Enable two-way data binding for form fields saved in a Vuex store.
- [dotenv](https://www.npmjs.com/package/dotenv-webpack) - dotenv-webpack wraps dotenv and Webpack.DefinePlugin. As such, it does a text replace in the resulting bundle for any instances of process.env.
- [vee-validate](https://logaretm.github.io/vee-validate) - Template Based Form Validation Framework for Vue.js
- [moment](https://momentjs.com) - Parse, validate, manipulate, and display dates and times in JavaScript.
- [express](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js (for production)
- [vuetify](https://vuetifyjs.com/en) - Vuetify is a Vue UI Library with beautifully handcrafted Material Components. No design skills required — everything you need to create amazing applications is at your fingertips

 
 ### Run Project
For starting serve client side app, just you need to run this command
 ```sh
$ npm run dev
```

All done! it should running on http://localhost:3000

# Tech

- Phpstorm
- Vue.js
- Tinkerwell
- Postman
- Webpack
- Vuedev Chrome tools
- MacOS

# User Interface and application logic



 ### Login Page

This is login page. 

![login](static/img/rtm-login.png "Login Page")


### Register Page

This is register page. You can create your user and sign into the system.

![register](static/img/rtm-register.png "Register Page")

### Change Password Page

This is change password page. You just write your e-mail and new password, just it. You don't need to check your e-mail. (In real life of course we should make confirmation via email or sms, but this is test project)

![change_password](static/img/rtm-change-password.png "Change Password Page")

### Dashboard Page

This is dashboard page. You can see your suggestions, followed videos and other types. When you are new user, you should follow movie,genre or star to see your suggestions.

Be careful! The movie search bar is not working. It's fake and just user interface.

![dashboard-1](static/img/rtm-1.png "Dashboard 1 Page")

You can see other videos in the system, by genre, star, year, followed and rate.

![dashboard-2](static/img/rtm-2.png "Dashboard 2 Page")


![dashboard-3](static/img/rtm-3.png "Dashboard 3 Page")

When you are hover on the movie box, you see details about related movie.

![movie-container](static/img/rtm-movie-container.png "Movie Contaniner Component")


### Follow Genres Page

When you are click follow genre button onto the top navigation bar, you will see all genres, and you can follow what you want. If you click some genre, the system will suggest movies kind by genres. 

![follow_genres](static/img/rtm-follow-genres.png "Follow Genres Page")

### Follow Stars Page

When you are click follow star button onto the top navigation bar, you will see all stars, and you can follow what you want. If you click some stars, the system will suggest movies kind by stars. 

![follow_stars](static/img/rtm-follow-stars.png "Follow Stars Page")






### About me
Thanks for reading.

- I'm Akin Gundogdu
- Email : akin-gundogdu@hotmail.com
- Linkedin : https://www.linkedin.co.uk/in/akingundogdu


   [client-online]: <https://rtm-app-client.herokuapp.com>
   [backend-online]: <http://rtm-app-api.herokuapp.com/>
   [backend-source-code]: <https://gitlab.com/akingundogdu/rtm-api.git>
   [client-source-code]: <https://gitlab.com/akingundogdu/rtm-app.git>
   [RubyGem]: <http://rubygems.org/>
   [Ruby]: <https://rubyonrails.org/>
